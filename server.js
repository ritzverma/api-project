const express = require('express');

const app = express();
const bodyparser = require("body-parser");
const indexRouter = require('./app/routes/index');
const resultRouter = require('./app/routes/result')
const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log("Node application listening on port 5000");
});
// Body Parser Middleware
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/getQuestions', indexRouter);
app.use('/getResult', resultRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send(err);
});
