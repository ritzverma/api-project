const mongoose = require('mongoose');
const Config = require('../../config');
const Connection = require('../database/connection');

require('../models/questionnaire');
const questionnaireData = mongoose.model('questionnaireData');

var QuestionsController = {};

QuestionsController.getData = function (req, res, next) {
    const db = Connection.connect(Config.databaseDetails.dbName);
    if (db !== "") {
        questionnaireData.findOne(function (err, questionsInfo) {
            if (err) {
                return console.error('Error occured while fetching Data from Mongo');
            }
            res.send(questionsInfo);
        });
    }
};

module.exports = QuestionsController;


