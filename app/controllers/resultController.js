const mongoose = require('mongoose');
const Config = require('../../config');
const Connection = require('../database/connection');

require('../models/resultModel');
const resultData = mongoose.model('resultData');

var ResultController = {};

ResultController.getData = function (req, res, next) {
    const db = Connection.connect(Config.databaseDetails.dbName);
    if (db !== "") {
        resultData.findOne(function (err, resultData) {
            var correctAnswerCount = 0;
            req.body.map((d, i) => {
                resultData.questions.map(el => {
                    if (d.id === el.id) {
                        correctAnswerCount = el.correctIndex === d.selectedAnswerIndex ? correctAnswerCount + 1 : correctAnswerCount;
                    }
                })
            })
            if (err) {
                return console.error('Error occured while fetching Data from Mongo');
            }
            res.send({ correctAnswerCount });
        });
    }
};

module.exports = ResultController;


