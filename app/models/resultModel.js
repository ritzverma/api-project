const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const questions = new Schema({
    id: Number,
    correctIndex: Number,
    question: {
        type: String,
        required: true,
        select: false
    },
    selectedIndex: {
        type: Number,
        required: true,
        select: false
    },
    options: {
        type: Array,
        required: true,
        select: false
    }
});

const resultData = new Schema({
    questions: [questions]

}, { collection: "quizQuestions" });



mongoose.model('resultData', resultData);