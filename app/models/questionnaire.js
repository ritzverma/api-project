const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const questions = new Schema({
    id: Number,
    question: String,
    options: [String],
    correctIndex: {
        type: Number,
        required: true,
        select: false
    },
    selectedIndex: Number
});

const questionnaireData = new Schema({

    questions: [questions]

}, { collection: "quizQuestions" });



mongoose.model('questionnaireData', questionnaireData);