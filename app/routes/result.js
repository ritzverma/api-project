'use strict';
const express = require('express');
const router = express.Router();
const ResultController = require('../controllers/resultController');

router.post('/', ResultController.getData);

module.exports = router;
