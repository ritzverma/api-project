'use strict';
const express = require('express');
const router = express.Router();
const QuestionsController = require('../controllers/questionsController');

router.get('/', QuestionsController.getData);

module.exports = router;
